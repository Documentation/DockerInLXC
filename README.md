# Docker In LXC

Append the following to `/etc/pve/lxc/<vmid>.conf` of an unpriviledged containers.

```
features: keyctl=1,nesting=1
```